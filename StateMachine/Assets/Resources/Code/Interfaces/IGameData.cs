﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Resources.Code.Interfaces
{
    public interface IGameData
    {
        /// <summary>
        /// Reset game parameter to initial value
        /// </summary>
        void ResetGameData();
    }
}
